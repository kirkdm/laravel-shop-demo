<?php

class Order extends Eloquent {

	// Don't forget to fill this array
	protected $fillable = ['user_id', 'shipping_address', 'billing_address', 'total'];

	public function products() {
		return $this->belongsToMany("Product", "orders_products");
	}

	public function user() {
		return $this->belongsTo("User");
	}

	static public function makeFromCart(UserProductCartCollection $cart)
	{
		$order = Order::create([
			'user_id' => Auth::id(),			
			'shipping_address' => '...',
			'billing_address' => '...',
			'total' => $cart->cartTotal(),
		]);

		foreach ($cart as $product) {
			$order->products()->attach($product->id);
		}

		$order->products;

		return $order;
	}
}