<?php 

class PaymentProcessor
{
	function startPayment($amount, $returnUrl, $cancelUrl, $currency = "NZD")
	{
		$purchase = Omnipay::purchase([
			'amount' 		=> $amount,
			'currency' 		=> $currency,
			'cancelUrl'		=> $cancelUrl,
			'returnUrl'		=> $returnUrl,
			//'EmailAddress'	=> 'kirkie99@outlook.com',
			//'TransactionId'			=> uniqid(microtime()),
			//'description'	=> 'test-description',
		])->send();
		
		//return omnipay_debug($purchase);

		if(!$purchase->isRedirect()) {
			throw new UnexpectedVlueException("Omnipay gateway said:\n" . $purchase->getMessage());
		}

		return $purchase->getRedirectUrl();		
	}

	function isPaymentAccepted()
	{
		$confirm = Omnipay::completePurchase([
				'result' => Input::get('result')
			])->send();

		return $confirm->isSuccessful();
	}


}