<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Shop Demo</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/products">Products</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('cart.index') }}">
          <span class="glyphicon glyphicon-shopping-cart"></span>
          Cart
          <span class="badge">{{ $cartCount }}</span>
        </a></li>
      </ul>
      @if( ! Auth::check())
        @if( Route::currentRouteName() !== 'login' && Route::currentRouteName() !== 'register' )


          {{ Form::open(
            array(
              'route' => 'login',
              'class' => 'navbar-form navbar-right',
              'role' => 'form'
            )
          ) }}
            <div class="form-group">
              {{ Form::text('email', NULL, array(
                'class' => 'form-control',
                'placeholder' => 'Email'
              )) }}
            </div>
            <div class="form-group">
              {{ Form::password('password', array(
                'class' => 'form-control',
                'placeholder' => 'Password'
              )) }}
            </div>
            {{ Form::submit('Login', array('class' => 'btn btn-success')) }}
            {{ link_to_route('register', 'Register', null, ['class' => 'btn btn-default']) }}
          {{ Form::close() }}
        
        @endif

      @else

        <div class="navbar-form navbar-right">
          {{ link_to_route('logout', "Logout", NULL, array(
            'class' => 'btn btn-default'
          )) }}
        </div>
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="#">{{ Auth::user()->email }}</a>
          </li>
        </ul>

      @endif
    </div><!--/.navbar-collapse -->
  </div>
</div>