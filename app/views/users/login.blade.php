@extends('layouts.master')

@section('page-title') @parent
Login
@stop

@section('content')

<div class="container">

	<h2>Login</h2>
	{{ Form::open([
		'route' => 'authenticate'
	]) }}

		@if (isset($error))
			<div class="alert alert-warning" role="alert">{{ $error }}</div>
		@endif

		<div class="form-group">
			{{ Form::label('email', 'Email') }}
			{{ Form::text('email', NULL, array(
				'placeholder' => 'email',
				'class' => 'form-control'
			)) }}
		</div>

		<div class="form-group">
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password', array(
				'placeholder' => 'password',
				'class' => 'form-control'
			)) }}
		</div>

		<div class="checkbox">
			{{ Form::label('remember_me', "Keep me logged in for five years") }}
			{{ Form::checkbox('remember_me', true, false) }}
		</div>


		<div class="form-group">
			{{ Form::submit('Log In', array(
				'class' => 'btn btn-primary'
			)) }}
		</div>
	{{ Form::close() }}

</div> <!-- /container -->

@stop