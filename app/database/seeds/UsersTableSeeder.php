<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('users')->truncate();

		User::create([
			'email' => 'alice@alice.com',
			'password' => Hash::make('alice'),
		]);

		foreach(range(1, 10) as $index)
		{
			$email = $faker->safeEmail;
			User::create([
				'email' => $email,
				'password' => Hash::make($email),
			]);
		}
	}

}